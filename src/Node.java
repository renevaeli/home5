public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;
   private static int pos;

   Node (String n, Node d, Node r) {
      setName (n);
      setFirstChild (d);
      setNextSibling (r);
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   public void setFirstChild(Node firstChild) {
      this.firstChild = firstChild;
   }

   public Node getFirstChild() {
      return firstChild;
   }

   public void setNextSibling(Node nextSibling) {
      this.nextSibling = nextSibling;
   }

   public Node getNextSibling() {
      return nextSibling;
   }

   public boolean hasNext() { return (getNextSibling() != null); }

   public boolean hasChild() {
        return (getFirstChild() != null);
    }

   public static String charAt(String s, int i) {
       return Character.toString(s.charAt(i));
   }

   public static Node parsePostfix (String s) {
       Node root = new Node("", null, null);

       pos = 0;

       if (s.contains("{") || s.contains("}") || s.contains(" ") || s.contains("\t")) {
           throw new RuntimeException("Etteantud sõne sisaldab keelatud sümboleid.");
       }

       if (!(s.contains("(") || s.contains(")")) && s.contains(",")) {
           throw new RuntimeException("Juurtipul ei saa olla naabrit.");
       }

       //kas üldse sisaldab sulge? Kui ei, siis tagasta kogu string
       if (s.contains("(") && s.contains(")")) {
           if (bracketsAreCorrect(s)) {
               root = processString(s);
           }
       } else {
           root.setName(s);
       }
       return root;
   }

   public static boolean bracketsAreCorrect(String s) {
       //tegeliklut võiks enamus vigadekontrolli teostada siin sees, aga pole aega refaktoreerida
       int countLeft = 0;
       int countRight = 0;

       for (int i = 0; i < s.length(); i++) {
           if (charAt(s, i).equals("(")) {
               countLeft++;
           }
           if (charAt(s, i).equals(")")) {
               countRight++;
           }
           if (countRight > countLeft) {
               throw new RuntimeException("Etteantud sõne struktuur on vigane");
           }
       }

       if (countLeft == countRight) {
           return true;
       } else {
           throw new RuntimeException("Etteantud sõne struktuur on vigane");
       }

   }

   public static Node processString(String s) {
       Node node = new Node("", null, null);
       StringBuffer b = new StringBuffer();

       String divs = "(,)";

       //in case parsing firstchild with no child, or parsing sibling
       while (!divs.contains(charAt(s, pos))) {
           b.append(charAt(s, pos));
           pos++;
       }

       if (charAt(s,pos).equals("(")) {
           pos++;
           if (charAt(s,pos).equals(",") || charAt(s,pos).equals(")")) {
               throw new RuntimeException("Tühi element pole lubatud"); }
           node.setFirstChild(processString(s));

           while (!divs.contains(charAt(s, pos))) {
               b.append(charAt(s, pos));
               pos++;
               if (s.length() == pos) break;
           }
       }
       if (pos < s.length()) {
           if (charAt(s,pos).equals(",")) {
               pos++;

               if (charAt(s,pos).equals(",") || charAt(s,pos).equals(")")) {
                   throw new RuntimeException("Tühi element pole lubatud");
               }
               node.setNextSibling(processString(s));
           }
           if (charAt(s,pos).equals(")")) {
               pos++;
               if (divs.contains(charAt(s, pos))) {
                   throw new RuntimeException("Tühi element pole lubatud");
               }
           }
       }

       node.setName(b.toString());
       return node;
   }

   public String leftParentheticRepresentation() {
      StringBuffer b = new StringBuffer();

      b.append(this.getName());

      if (this.hasChild()) {
          //teen esimese iteratsiooni eraldi, et lõppu ei tuleks lisasulgu
          b.append("(" + this.getFirstChild().getName());
          b.append(processNodes(this.getFirstChild()));
      }
      return b.toString();
   }

   public String processNodes(Node node){
       StringBuffer b = new StringBuffer();

       if (node.hasChild()) {
           b.append("(" + node.getFirstChild().getName());
           b.append(processNodes(node.getFirstChild()));
       }
       if (node.hasNext()) {
           b.append("," + node.getNextSibling().getName());
           b.append(processNodes(node.getNextSibling()));
       } else {
           b.append(")");
       }
       return b.toString();
   }

   public static void main (String[] param) {
       String test = "(((1X)C1,D1)B1,(F1,G1)E1)A1";
       Node testnode = Node.parsePostfix(test);
       System.out.println(testnode.leftParentheticRepresentation());

       String s = "((F,G)E,(C,D)B)A";
       Node t = Node.parsePostfix (s);
       String v = t.leftParentheticRepresentation();
       System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }

}

